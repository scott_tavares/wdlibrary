package wdlibraryprocessreport;

import edu.brown.wd_library.report.Config;
import edu.brown.wd_library.report.WDLibraryProcessReport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
public class WDLibraryProcessReportRunner {

  /**
   * @param args the command line arguments
   * @throws IOException
   * @throws ParseException
   */
  public static void main(String[] args)
      throws IOException, ParseException {

    // Get the logger
    Logger LOG = Logger
        .getLogger(WDLibraryProcessReport.class.getName());

    // File object for the output reports
    File file;

    // PrintWriters
    Writer printWriterErrorsReport;
    Writer printWriterWorkdayReport;
    Writer printWriterLibraryReport;

    // TODO: Reminder: The properties file path will need to be changed for
    // production
    // The properties file name
    final String PROPERTIES_FILE_NAME
        = "wd_library_config.properties";

    // Instantiate the properties
    Properties properties = new Properties();

    // The number of errors
    int errorCount;

    // Reusable error message
    String errorMessage;

    // The output device (file, console)
    String outputDevice;

    // Load the properties file
    try {
      properties.load(new FileInputStream(PROPERTIES_FILE_NAME));
    } catch (IOException e) {
      throw e;
    }

    ////////////////////////////////////////////////////////////////////////
    // Configure the PrintWriter(s) that are used to output all generated
    // Information.
    // Check for a valid config file
    if (new Config(properties).isValid()) {

      // Get the property of the key "output_device"
      outputDevice = properties.getProperty("output_device");

      // Set the PrintWriters to print to file
      if (outputDevice.equalsIgnoreCase("file")) {
        file = new File(properties
            .getProperty("file_out_error_report"));

        printWriterErrorsReport = new PrintWriter(file);

        file = new File(properties
            .getProperty("file_out_workday_report"));

        printWriterWorkdayReport = new PrintWriter(file);

        file = new File(properties
            .getProperty("file_out_library_report"));

        printWriterLibraryReport = new PrintWriter(file);

      } else // Set the PrintWriters to print to the console
      {
        printWriterErrorsReport = new PrintWriter(System.out);
        printWriterWorkdayReport = new PrintWriter(System.out);
        printWriterLibraryReport = new PrintWriter(System.out);
      }

      ////////////////////////////////////////////////////////////////////
      // Instantiate the WDLibraryProcessReport object
      try {
        // Creating an instance of the WDLibraryProcessReport class
        // reads the data file and stores each line in the file
        // into a Record class.
        // Each record instance is then stored in an ArrayList.
        // During this process, a large number of data validation checks
        // are performed. All errors are collected and stored in either
        // the Vendor or Invoice classes.
        WDLibraryProcessReport wdLibraryProcessReport
            = new WDLibraryProcessReport(properties);

        // If errors are found, an error report is generated. Otherwise
        // a human readable report and a data file is created.
        if (wdLibraryProcessReport.hasErrors()) {
          // Get the error count
          errorCount = wdLibraryProcessReport.getErrorCount();

          // Create the error message
          errorMessage = errorCount
              + (errorCount > 1 ? " Errors" : " Error")
              + " found while processing apvouch.txt. "
              + "\nPlease resolve them before continuing.";

          // Produce Error Report
          wdLibraryProcessReport
              .printErrorsReport(printWriterErrorsReport);

          // Write a failed execution message to both reports
          printWriterWorkdayReport.write(errorMessage);
          printWriterLibraryReport.write(errorMessage);

          // Write a failed execution message to the console
          if (outputDevice.equalsIgnoreCase("file")) {
            System.out.println(errorMessage);
          }

          // Close the PrintWriters
          printWriterWorkdayReport.close();
          printWriterLibraryReport.close();
          printWriterErrorsReport.close();
        } else {

          // Produce the WorkDay Report
          wdLibraryProcessReport
              .printWorkDayReport(printWriterWorkdayReport);

          // Produce the Library Report
          wdLibraryProcessReport
              .printLibraryReport(printWriterLibraryReport);

          // Write a success message to the errors file
          printWriterErrorsReport.write("Execution completed "
              + "successfully with no errors.\n");

          // Close the PrintWriters
          printWriterWorkdayReport.close();
          printWriterLibraryReport.close();
          printWriterErrorsReport.close();
        }
      } catch (FileNotFoundException | ParseException e) {
        throw e;
      }
    } else {
      // Log properties error
      LOG.log(Level.WARNING,
          "Errors have occurred in the properties file. "
          + "Please resolve them before continuing.");
    }
  }
}
