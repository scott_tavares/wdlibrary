package wdlibraryprocessreport;

import java.io.IOException;
import java.text.ParseException;

/**
 * This class is purely so Workday will recognize the jar. It's name conforms to
 * what the WorkDay process is expecting.
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
public class WDLibraryProcessrpt
{
    public static void main(String[] args) throws IOException, ParseException
    {
        WDLibraryProcessReportRunner.main(args);
    }
}
