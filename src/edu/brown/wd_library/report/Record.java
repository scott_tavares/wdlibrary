package edu.brown.wd_library.report;

/**
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
final class Record
{
    private RecordType recordType;
    private String theRecord;

    private int lineNumber;

    public enum RecordType
    {
        VENDOR,
        INVOICE,
        PCARD,
        WIRE,
        NONE,
        ALLDONE
    }

    Record(String theRecord, int lineNumber)
    {
        // Set the record and line number
        setTheRecord(theRecord);
        setLineNumber(lineNumber);

        // Assign Record Type (Order dependent)
        if (getTheRecord().substring(32, 37).toUpperCase().contains("PCARD")
                || getTheRecord().substring(56, 61).toUpperCase()
                        .contains("PCARD"))
        {
            setRecordType(RecordType.PCARD);
        } else if (getTheRecord().toUpperCase().contains("NONE"))
        {
            setRecordType(RecordType.NONE);
        } else if (getTheRecord().substring(7, 14).toUpperCase()
                .contains("VENNAME"))
        {
            setRecordType(RecordType.VENDOR);
        } else if (getTheRecord().substring(56, 60).toUpperCase()
                .contains("WIRE")
                || getTheRecord().substring(32, 36).toUpperCase()
                        .contains("WIRE"))
        {
            setRecordType(RecordType.WIRE);
        } else if (getTheRecord().substring(7, 14).toUpperCase()
                .contains("ALLDONE"))
        {
            setRecordType(RecordType.ALLDONE);
        } else
        {
            setRecordType(RecordType.INVOICE);
        }
    }

    // Mutator methods

    /**
     * @param theRecord The theRecord to set
     */
    private void setTheRecord(String theRecord)
    {
        this.theRecord = theRecord;
    }

    /**
     * @param type The recordType to set
     */
    private void setRecordType(RecordType type)
    {
        this.recordType = type;
    }

    /**
     * @param lineNumber The line number to set
     */
    private void setLineNumber(int lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    // Accessor methods

    /**
     * @return The theRecord
     */
    private String getTheRecord()
    {
        return theRecord;
    }

    /**
     * @return The recordType
     */
    RecordType getRecordType()
    {
        return recordType;
    }

    /**
     * @return The record's line number
     */
    int getLineNumber()
    {
        return lineNumber;
    }

    /**
     * @return The length of the record
     */
    private int getLength()
    {
        return getTheRecord().length();
    }

    /**
     * Check for a valid Record length
     *
     * @return If it's valid
     */
    boolean isValid()
    {
        return getLength() > 0 && getLength() <= 250;
    }

    @Override
    public String toString()
    {
        return getTheRecord();
    }
}
