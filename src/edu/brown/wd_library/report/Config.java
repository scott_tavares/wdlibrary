package edu.brown.wd_library.report;

import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * To add a new property to the properties file, you must also validate it.
 * Follow the "TODO" blue marks on the side for a step by step.
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
public class Config
{
    private boolean outputDevice;
    private boolean logLevel;
    private boolean fileIn;
    private boolean libraryReport;
    private boolean workdayReport;
    private boolean errorReport;
    private boolean logFile;
    private boolean containsBadKey;

    private Properties properties;

    private static final Logger LOG = Logger
            .getLogger(WDLibraryProcessReport.class.getName());

    // TODO: Step 1 - Add the new KeyType
    private enum KeyType
    {
        OUTPUT_DEVICE,
        LOG_LEVEL,
        FILE_IN,
        LIBRARY_REPORT,
        WORKDAY_REPORT,
        ERROR_REPORT,
        LOG_FILE,
        COMMENT,
        INVALID
    }

    // TODO: Step 2 - Set the property's default state
    public Config(Properties properties)
    {
        // Default the fields
        setOutputDevice(true);
        setLogLevel(true);
        setFileIn(true);
        setLibraryReport(true);
        setWorkdayReport(true);
        setErrorReport(true);
        setLogFile(true);
        setContainsBadKey(false);
        setProperties(properties);

        // Validate the Config
        validate();
    }

    /**
     * Validate the Config file
     */
    private void validate()
    {
        // Property names
        Enumeration<?> e = getProperties().propertyNames();

        // Iterate through the properties file
        while (e.hasMoreElements())
        {
            String property;

            // Get the keyString
            String keyString = e.nextElement().toString().toLowerCase();

            // Set the KeyType from the keyString
            KeyType key = getKey(keyString);

            // Get the property from the keyString
            property = getProperties().getProperty(keyString).toLowerCase();

            // Switch the key
            switch (key)
            {
                // TODO: Step 3 - Add the KeyType to this switch and the
                // validation code for all its valid pairs
                case OUTPUT_DEVICE:
                    // Check for valid fields
                    if (!(property.equalsIgnoreCase("file") || property
                            .equalsIgnoreCase("console")))
                    {
                        // Log and set theOutputDevice to false
                        logInvalidProperty(property, keyString, "file, console");
                        setOutputDevice(false);
                    }
                    break;
                case LOG_LEVEL:
                    // Declare valid fields
                    int[] validFields = { 3200, 1000, 900, 800, 700, 600, 500,
                            400, 300, -3200 };

                    // Check for numeric
                    if (Util.isNumeric(property))
                    {
                        boolean valid = false;

                        // Check for a valid field
                        for (int field : validFields)
                        {
                            if (field == Integer.parseInt(property))
                            {
                                valid = true;
                                break;
                            }
                        }

                        if (!valid)
                        {
                            // Log and set the logLevel to false
                            logInvalidProperty(property, keyString,
                                    "3200, 1000, 900, 800, 700, 600, 500, 400, 300, -3200");
                            setLogLevel(false);
                        }
                    } else
                    {
                        // Log and set the logLevel to false
                        logInvalidProperty(property, keyString,
                                "Numeric value 3200, 1000, 900, 800, 700, 600, 500, 400, 300, -3200");
                        setLogLevel(false);
                    }
                    break;
                case FILE_IN:
                    // Check for valid file_in
                    if (!(property.startsWith("apvouch") && property
                            .endsWith(".txt")))
                    {
                        // Log and set the fileIn to false
                        logInvalidProperty(property, keyString, "apvouch.txt");
                        setFileIn(false);
                    }
                    break;
                case LIBRARY_REPORT:
                    // Check for a valid .txt document
                    if (!((property.startsWith("libraryreport") || property
                            .startsWith("inv_library")) && property
                            .endsWith(".txt")))
                    {
                        // Log and set the libraryReport to false
                        logInvalidProperty(property, keyString,
                                "Inv_Library.txt");
                        setLibraryReport(false);
                    }
                    break;
                case WORKDAY_REPORT:
                    // Check for a valid txt document
                    if (!((property.startsWith("workdayreport") || property
                            .startsWith("inv_lib_wdlibraryreport")) && property
                            .endsWith(".txt")))
                    {
                        // Log and set the workdayReport to false
                        logInvalidProperty(property, keyString,
                                "Inv_Lib_WDLibraryReport.txt");
                        setWorkdayReport(false);
                    }
                    break;
                case ERROR_REPORT:
                    // Check for a valid txt document
                    if (!(property.contains("libraryerrorreport") && property
                            .endsWith(".txt")))
                    {
                        // Log and set the errorReport to false
                        logInvalidProperty(property, keyString,
                                "LibraryErrorReport.txt");
                        setErrorReport(false);
                    }
                    break;
                case LOG_FILE:
                    // Check for valid log document
                    if (!(property.startsWith("wdlibraryprocessreport") && property
                            .endsWith(".log")))
                    {
                        // Log and set the logFile to false
                        logInvalidProperty(property, keyString,
                                "WDLibraryProcessReport.log");
                        setLogFile(false);
                    }
                    break;
                // Ignore commented lines
                case COMMENT:
                    break;
                case INVALID:
                    // Log and set the containsBadKey to true
                    LOG.log(Level.WARNING, "Key \"{0}\" is invalid\n",
                            keyString);
                    setContainsBadKey(true);
            }
        }
    }

    /**
     * Get the Key(type) from the keyString
     *
     * @param keyString String value of the key
     * @return The Key type
     */
    private KeyType getKey(String keyString)
    {
        KeyType key;

        // Check for a comment
        if (keyString.startsWith(";"))
        {
            keyString = ";";
        } else if (keyString.startsWith("#"))
        {
            keyString = "#";
        } else if (keyString.startsWith("!"))
        {
            keyString = "!";
        }

        // Set the Key type
        switch (keyString)
        {
            // TODO: Step 4 - Add the keyString to the switch and set the keyType
            case "output_device":
                key = KeyType.OUTPUT_DEVICE;
                break;
            case "log_level":
                key = KeyType.LOG_LEVEL;
                break;
            case "file_in":
                key = KeyType.FILE_IN;
                break;
            case "file_out_library_report":
                key = KeyType.LIBRARY_REPORT;
                break;
            case "file_out_workday_report":
                key = KeyType.WORKDAY_REPORT;
                break;
            case "file_out_error_report":
                key = KeyType.ERROR_REPORT;
                break;
            case "log_file":
                key = KeyType.LOG_FILE;
                break;
            case ";":
            case "#":
            case "!":
                key = KeyType.COMMENT;
                break;
            default:
                key = KeyType.INVALID;
        }

        return key;
    }

    /**
     * @param outputDevice The outputDevice to set
     */
    private void setOutputDevice(boolean outputDevice)
    {
        this.outputDevice = outputDevice;
    }

    /**
     * @param logLevel The logLevel to set
     */
    private void setLogLevel(boolean logLevel)
    {
        this.logLevel = logLevel;
    }

    /**
     * @param fileIn The fileIn to set
     */
    private void setFileIn(boolean fileIn)
    {
        this.fileIn = fileIn;
    }

    /**
     * @param libraryReport The libraryReport to set
     */
    private void setLibraryReport(boolean libraryReport)
    {
        this.libraryReport = libraryReport;
    }

    /**
     * @param workdayReport The workdayReport to set
     */
    private void setWorkdayReport(boolean workdayReport)
    {
        this.workdayReport = workdayReport;
    }

    /**
     * @param errorReport The errorReport to set
     */
    private void setErrorReport(boolean errorReport)
    {
        this.errorReport = errorReport;
    }

    /**
     * @param logFile The logFile to set
     */
    private void setLogFile(boolean logFile)
    {
        this.logFile = logFile;
    }

    /**
     * @param containsBadKey The containsBadKey to set
     */
    private void setContainsBadKey(boolean containsBadKey)
    {
        this.containsBadKey = containsBadKey;
    }

    /**
     * @param properties The properties to set
     */
    private void setProperties(Properties properties)
    {
        this.properties = properties;
    }

    /**
     * @return The properties
     */
    private Properties getProperties()
    {
        return properties;
    }

    /**
     * @return The outputDevice
     */
    private boolean isOutputDeviceValid()
    {
        return outputDevice;
    }

    /**
     * @return The logLevel
     */
    private boolean isLogLevelValid()
    {
        return logLevel;
    }

    /**
     * @return The fileIn
     */
    private boolean isFileInValid()
    {
        return fileIn;
    }

    /**
     * @return The libraryReport
     */
    private boolean isLibraryReportValid()
    {
        return libraryReport;
    }

    /**
     * @return The workdayReport
     */
    private boolean isWorkdayReportValid()
    {
        return workdayReport;
    }

    /**
     * @return The errorReport
     */
    private boolean isErrorReportValid()
    {
        return errorReport;
    }

    /**
     * @return The logFile
     */
    private boolean isLogFileValid()
    {
        return logFile;
    }

    /**
     * @return The if the config contains a bad key
     */
    private boolean doesContainBadKey()
    {
        return containsBadKey;
    }

    /**
     * @return If the Config is valid
     */
    public boolean isValid()
    {
        // TODO: Step 5 - Add the isValid() or similar method to this return
        return isOutputDeviceValid() && isLogLevelValid() && isFileInValid()
                && isLibraryReportValid() && isWorkdayReportValid()
                && isErrorReportValid() && isLogFileValid()
                && !doesContainBadKey();
    }

    /**
     * Reusable logger when a property is invalid
     *
     * @param property The property
     * @param keyString The key string
     * @param expected The expected property for the key
     */
    private void logInvalidProperty(String property, String keyString,
            String expected)
    {
        LOG.log(Level.WARNING,
                "Property \"{0}\" invalid for key: \"{1}\". Expected: \"{2}\"\n",
                new Object[] { property, keyString, expected });
    }
}
