package edu.brown.wd_library.report;

import edu.brown.wd_library.report.LibraryException.ErrorType;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class models a Vendor
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
class Vendor
{
    private static final Logger LOG = Logger
            .getLogger(WDLibraryProcessReport.class.getName());

    private final Record record;

    private String voucher;
    private String recordType;
    private String vendorCode;
    private String vendorName;

    private double invoiceAmount;
    private double discountAmount;
    private double shippingAmount;

    private final List<Invoice> invoices;
    private final List<LibraryException> errors;

    Vendor(Record record)
    {
        // Set the record
        this.record = record;

        invoices = new CopyOnWriteArrayList<>();
        errors = new CopyOnWriteArrayList<>();

        // Check that it has a valid length
        checkRecordLength(getRecord());

        // Set the Vendor's fields
        setVoucher(record.toString().substring(0, 7));
        setRecordType(record.toString().substring(7, 14));
        setVendorCode(record.toString().substring(19, 29));
        setVendorName(record.toString().substring(29, 59));
    }

    // Mutator Methods

    /**
     * Validate and add the Invoice to this Vendor
     *
     * @param invoice New invoice to add
     */
    void addInvoice(Invoice invoice)
    {
        String errorMessage = "Vendor and Invoice vouchers do not match: "
                + "Vendor: <" + getVoucher() + "> Invoice: <"
                + invoice.getVoucher() + ">";

        String nullErrorMessage = "Invoice Record is null";

        try
        {
            // Add the Invoice
            getInvoices().add(invoice);

            // Check if it matches the Invoice voucher
            if (invoice.getVoucher().equals(getVoucher()))
            {
                incrementAmount(invoice.getAmount());
                incrementShipingAmount(invoice.getShipping());
                incrementDiscountAmount(invoice.getDiscount());
            } else
            {
                // Add the mismatch error to the Vendor's errors list
                LibraryException e = new LibraryException(errorMessage, this,
                        invoice, ErrorType.VOUCHERS_DO_NOT_MATCH);

                LOG.log(Level.SEVERE,
                        "**** Vendor and Invoice vouchers do not match: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } catch (NullPointerException ex)
        {
            // Add the null Record error to the Vendor's errors list
            LibraryException e = new LibraryException(nullErrorMessage, this,
                    ErrorType.INVALID_INVOICE);

            LOG.log(Level.SEVERE,
                    "**** Invoice Record is null: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the voucher
     *
     * @param voucher The voucher to set
     */
    private void setVoucher(String voucher)
    {
        String errorMessage = "Invalid Voucher Length: <" + voucher + ">";

        String notNumericErrorMessage = "Expected numeric Voucher value, but saw <"
                + voucher.trim() + ">";

        int length = voucher.length();

        // Check for a valid length
        if (length <= 7 && length > 0)
        {
            // Check for numeric
            if (Util.isNumeric(voucher.trim()))
            {
                // Set the voucher
                this.voucher = voucher.trim();
            } else
            {
                // Add the numeric error to the Vendor's errors list
                LibraryException e = new LibraryException(
                        notNumericErrorMessage, this,
                        ErrorType.VOUCHER_NOT_NUMERIC);

                LOG.log(Level.SEVERE,
                        "**** Voucher value is not numeric: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } else
        {
            // Add the length error to the Vendor's errors list
            LibraryException e = new LibraryException(errorMessage, this,
                    ErrorType.INVALID_VOUCHER);

            LOG.log(Level.SEVERE,
                    "**** Invalid Voucher Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the record type
     *
     * @param recordType The recordType to set
     */
    private void setRecordType(String recordType)
    {
        String errorMessage =
               "Invalid Record Type Length: <" + recordType + ">";

        int length = recordType.length();

        // Check for a valid length
        if (length <= 7 && length > 0)
        {
            // Set the record type
            this.recordType = recordType.trim();
        } else
        {
            // Add the length error to the Vendor's errors list
            LibraryException e = new LibraryException(errorMessage, this,
                    ErrorType.INVALID_RECORD_TYPE);

            LOG.log(Level.SEVERE,
                    "**** Invalid Record Type Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the vendor code
     *
     * @param alternateVendorCode The vendorCode to set
     */
    private void setVendorCode(String alternateVendorCode)
    {
        String errorMessage = "Invalid Vendor Code Length: <"
                + alternateVendorCode + ">";

        int length = alternateVendorCode.length();

        // Check for a valid length
        if (length <= 10 && length > 0)
        {
            if (recordType.contains("VENNAME")
                    && alternateVendorCode.contains("PL-"))
            {
                // Set the vendor code with the provided code
                vendorCode = "S" + alternateVendorCode.trim();
            } else
            {
                // Set the vendor code as the default code
                vendorCode = "SPL-0009999";
            }
        } else
        {
            // Add the length error to the Vendor's errors list
            LibraryException e = new LibraryException(errorMessage, this,
                    ErrorType.INVALID_VENDOR_CODE);

            LOG.log(Level.SEVERE, "**** Invalid Alternate Vendor Code "
                    + "Length: {0}****\n", e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the vendor name
     *
     * @param vendorName The vendorName to set
     */
    private void setVendorName(String vendorName)
    {
        String errorMessage = "Invalid Vendor Name Length: <" + vendorName
                + ">";

        int length = vendorName.length();

        // Check for a valid length
        if (length <= 30 && length > 0)
        {
            // Set the vendor name
            this.vendorName = vendorName;
        } else
        {
            // Add the length error to the Vendor's errors list
            LibraryException e = new LibraryException(errorMessage, this,
                    ErrorType.INVALID_VENDOR_NAME);

            LOG.log(Level.SEVERE, "**** Invalid Vendor Name "
                    + "Length: {0}****\n", e.toString());

            getErrors().add(e);
        }
    }

    // Accessor Methods

    /**
     * @return The list of invoices
     */
    List<Invoice> getInvoices()
    {
        return invoices;
    }

    /**
     * @return The voucher
     */
    String getVoucher()
    {
        return voucher;
    }

    /**
     * @return The vendorCode
     */
    String getVendorCode()
    {
        return vendorCode;
    }

    /**
     * @return The vendorName
     */
    String getVendorName()
    {
        return vendorName;
    }

    /**
     * @return The invoiceAmount
     */
    double getInvoiceAmount()
    {
        return invoiceAmount;
    }


    /**
     * @return The shippingAmount
     */
    double getShippingAmount()
    {
        return shippingAmount;
    }

    /**
     * @return The record
     */
    private Record getRecord()
    {
        return record;
    }

    /**
     * @return The list of errors
     */
    private List<LibraryException> getErrors()
    {
        return errors;
    }

    /**
     * Calculate the amount of errors this Vendor has and all its Invoices
     *
     * @return The amount of errors
     */
    int getErrorCount()
    {
        // Set the count to the amount of Vendor errors
        int errorCount = getErrors().size();

        // Iterate through all this Vendor's Invoices
        for (Invoice invoice : getInvoices())
        {
            // Increment the error count if an error is present
            if (invoice.hasErrors())
            {
                errorCount += invoice.getErrorCount();
            }
        }

        return errorCount;
    }

    /**
     * @return The line number
     */
    int getLineNumber()
    {
        return getRecord().getLineNumber();
    }

    // Invoice Accessors

    /**
     * Grab the Account Number from index 0
     * of the Invoices list
     *
     * @return The account number
     */
    String getAccountNumber()
    {
        String account = null;

        if (this.hasInvoices())
        {
            account = getInvoices().get(0).getAccount();
        }

        return account;
    }

    /**
     * Grab the Invoice Number from index 0
     * of the Invoices list
     *
     * @return The invoice number
     */
    String getInvoiceNumber()
    {
        String invoiceNumber = null;

        if (this.hasInvoices())
        {
            invoiceNumber = getInvoices().get(0).getInvoiceNumber();
        }

        return invoiceNumber;
    }

    /**
     * Grab the Invoice Date from index 0
     * of the Invoices list
     *
     * @return The invoice date
     */
    String getInvoiceDate()
    {
        String invoiceDate = null;

        if (this.hasInvoices())
        {
            invoiceDate = getInvoices().get(0).getInvoiceDate();
        }

        return invoiceDate;
    }

    /**
     * @param invoiceAmount The amount to increment
     */
    private void incrementAmount(double invoiceAmount)
    {
        this.setInvoiceAmount(this.getInvoiceAmount() + invoiceAmount);
    }

    /**
     * @param shippingAmount The shipping amount to increment
     */
    private void incrementShipingAmount(double shippingAmount)
    {
        this.setShippingAmount(this.getShippingAmount() + shippingAmount);
    }

    /**
     * @return The total amount of all this vendor's invoices
     */
    double calcTotalAmount()
    {
        return getInvoiceAmount() + getShippingAmount() + getDiscountAmount();
    }

    /**
     * Write out the errors for this Vendor and all its Invoices
     *
     * @param printWriter The errors Writer
     */
    void printErrors(Writer printWriter, int[] count) throws IOException
    {
        // Write out each error from this Class
        for (LibraryException error : getErrors())
        {
            error.print(printWriter, count);
        }

        // Write out each error from its Invoices
        for (Invoice invoice : getInvoices())
        {
            invoice.printErrors(printWriter, count);
        }
    }

    /**
     * Check if this Vendor or any of its Invoices has errors
     *
     * @return If there are errors
     */
    boolean hasErrors()
    {
        boolean hasErrors = false;

        // Check for errors in this Class
        if (getErrors().size() > 0)
        {
           hasErrors = true;
        } else
        {
            // If none found, iterate through all its Invoices
            for (Invoice invoice : getInvoices())
            {
                // If an error is found, return true
                if (invoice.hasErrors())
                {
                    hasErrors = true;
                    break;
                }
            }
        }

        return hasErrors;
    }

    /**
     * Check if this Vendor has Invoices
     *
     * @return If this has Invoices
     */
    boolean hasInvoices()
    {
        return getInvoices().size() > 0;
    }

    /**
     * Check the record for a valid length
     *
     * @param record The Record
     */
    private void checkRecordLength(Record record)
    {
        // Check the Record length
        if (!record.isValid())
        {
            LibraryException e =
                    new LibraryException("Record length larger than 250",
                        this, ErrorType.INVALID_RECORD);

            LOG.log(Level.SEVERE,
                    "**** Record length larger than 250: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    @Override
    public String toString()
    {
        return getRecord().toString();
    }

    private void incrementDiscountAmount(double discount)
    {
        this.setDiscountAmount(this.getDiscountAmount() + discount);
    }

    /**
     * @return the discountAmount
     */
    double getDiscountAmount()
    {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    private void setDiscountAmount(double discountAmount)
    {
        this.discountAmount = discountAmount;
    }

    /**
     * @param invoiceAmount the invoiceAmount to set
     */
    private void setInvoiceAmount(double invoiceAmount)
    {
        this.invoiceAmount = invoiceAmount;
    }

    /**
     * @param shippingAmount the shippingAmount to set
     */
    private void setShippingAmount(double shippingAmount)
    {
        this.shippingAmount = shippingAmount;
    }
}
