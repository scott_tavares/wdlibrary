package edu.brown.wd_library.report;

import edu.brown.wd_library.report.Record.RecordType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class servers as the public API for the WDLibrary Process application.
 * <p>
 * The API consists of one constructor and eight public methods. The constructor
 * takes takes a Properties object as a parameter. The constructor uses the
 * Properties object to find the input data file ("file_in" property in the
 * wd_library_config.properties file) and read its contents and parse its data
 * into the appropriate classes for later use.
 * <p>
 * There are two methods for error handling; hasError() and getErrorCount().
 * These methods should be called after this class has been instantiated. They
 * report if there have been any errors and how many of them there are after the
 * input file has been parsed from within the constructor.
 * <p>
 * The remaining six methods are used to output the data read in from the input
 * file to the required formats. Where that data is directed to is dependent on
 * how the Writer classes are configured that are passed into the null {@link #printErrorsReport(Writer printWriter)},
 * {@link #printLibraryReport(Writer printWriter)} and
 * {@link #printWorkDayReport(Writer printWriter)} methods as parameters.
 * <p>
 * The null {@link #printErrorsReport()},
 * {@link #printLibraryReport()} and {@link #printWorkDayReport()} methods
 * return the same data as the three methods listed above but do not take a
 * Writer object as a parameter and returns a formated string instead.
 *
 * <p>
 * Any other classes and methods in the library are meant to be "black box". It
 * was with this reasoning that all other classes in the package have access
 * modifiers of package private(default). This precludes any other classes from
 * outside this package from manipulating them directly.
 * <p>
 * The best was to understand how this class is used is to examining the
 * {@link wdlibraryprocessreport.WDLibraryProcessReportRunner} class.
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
public class WDLibraryProcessReport {

  private static final Logger LOG = Logger
          .getLogger(WDLibraryProcessReport.class.getName());

  private final static String INPUT_FILE_PROPERTY_NAME = "file_in";

  // JDK Classes
  private Properties properties;
  private final Locale defaultLocale;
  private final NumberFormat currencyFormatter;
  private File inputFile;

  // Lists
  private final List<Vendor> vendors;
  private final List<Record> records;

  // Vendor holder for input processing
  private Vendor currentVendor;

  // The output device -- file, console
  private String outputDevice;

  private int batchCount = 0;
  private double batchAmount = 0;

  /**
   * @param properties The properties
   * @throws IOException
   * @throws ParseException
   */
  public WDLibraryProcessReport(Properties properties)
          throws IOException, ParseException {
    // Load the properties file.
    setProperties(properties);

    // Setup Logging
    LOG.addHandler(new FileHandler(properties.getProperty("log_file")));
    setLogLevel(LOG, Integer.parseInt(properties.getProperty("log_level")));

    // Setup currency formatter
    defaultLocale = new Locale("en", "US", "USD");
    currencyFormatter = NumberFormat.getCurrencyInstance(defaultLocale);

    // Initialize list classes
    vendors = new CopyOnWriteArrayList<>();
    records = new CopyOnWriteArrayList<>();

    // Set the output device
    setOutputDevice(getProperties().getProperty("output_device"));

    // Set the input file
    setInputFile(new File(getProperties()
            .getProperty(INPUT_FILE_PROPERTY_NAME)));

    // Load all the data and store it in their respective classes
    parseInputDataFile();
  }

  /**
   * Load the records into the records List
   *
   * @throws FileNotFoundException
   */
  private void loadRecords() throws FileNotFoundException {
    Record record;
    int lineNumber = 1;
    setBatchCount(0);

    // Read all records into records List
    try (Scanner inputFileScanner = new Scanner(getInputFile())) {
      // Read all Records into the records List
      while (inputFileScanner.hasNext()) {
        // Create a new Record and add it
        record = new Record(inputFileScanner.nextLine(), lineNumber);
        getRecords().add(record);

        // TODO: Should RecordType.NONE also be included here?
        if (record.getRecordType() != RecordType.PCARD
                && record.getRecordType() != RecordType.WIRE) {
          setBatchCount(getBatchCount() + 1);
        }

        // Increment line number
        lineNumber++;
      }
    }
  }

  /**
   * Parse the input data into a List, skip unwanted records, and log important
   * information
   *
   * @throws ParseException
   * @throws IOException
   */
  private void parseInputDataFile()
          throws ParseException, IOException {
    Invoice invoice;
    RecordType recordType;

    // Load the records
    try {
      loadRecords();
    } catch (FileNotFoundException e) {
      // File was not found
      LOG.throwing(this.getClass().getName(), "parseInputDataFile()", e);
      throw e;
    }

    // Load the vendors and invoices.
    for (Record record : getRecords()) {
      recordType = record.getRecordType();

      // Ignore all recordTypes except
      // VENDOR and INVOICE
      switch (recordType) {
        case VENDOR:
          setCurrentVendor(new Vendor(record));
          getVendors().add(getCurrentVendor());
          break;
        case INVOICE:
          if (getCurrentVendor() != null) {
            invoice = new Invoice(record);
            setBatchAmount(getBatchAmount() + invoice.calcTotal());
            getCurrentVendor().addInvoice(invoice);
          }
          break;
        default:
          LOG.log(Level.INFO,
                  "Ignore record: {0}||{1}",
                  new Object[]{
                    record.getRecordType(),
                    record.toString()
                  });
      }
    }

    // Remove unwanted Vendors
    cleanVendorList();

    // Log the number of Records
    LOG.log(Level.INFO, "Number of records read: {0}\n",
            getRecords().size());

    // Log the number of Vendors
    LOG.log(Level.INFO, "Number of Vendors: {0}\n\n", getVendors().size());

    // Log vendor information
    for (Vendor vendor : getVendors()) {
      LOG.log(Level.FINE, "Vendor {0} has {1} invoices.\n",
              new Object[]{
                vendor.getVendorName(), vendor.getInvoices().size()
              });
    }
  }

  /**
   * Iterate through all the errors and write it to file or console
   *
   * @param printWriter The errors Writer
   * @throws IOException Thrown if file cannot be read
   */
  public void printErrorsReport(Writer printWriter) throws IOException {
    int[] count = {1};

    // Write the count
    printWriter.write("Total number of errors: " + getErrorCount() + "\n\n");

    // Iterate through each vendor and
    // write each error from it and its Invoices
    for (Vendor vendor : getVendors()) {
      if (vendor.hasErrors()) {
        vendor.printErrors(printWriter, count);
      }
    }

    printWriter.close();
  }

  /**
   * @return The report as a String
   * @throws IOException
   */
  public String printErrorsReport() throws IOException {
    Writer pw;
    ByteArrayOutputStream ops;

    ops = new ByteArrayOutputStream();
    pw = new PrintWriter(ops, true);
    printErrorsReport(pw);

    return ops.toString();
  }

  /**
   * Write out the WorkDay report to file or console
   *
   * @param printWriter The Writer
   * @throws ParseException
   * @throws IOException
   */
  public void printWorkDayReport(Writer printWriter)
          throws ParseException, IOException {
    double totalAmount = 0; // Running total

    // Write out the header
    printWriter.write(String.format("%-12s", "Vendor Id"));
    printWriter.write(String.format("%-30s", "Vendor Name"));
    printWriter.write(String.format("%-12s", "Voucher"));
    printWriter.write(String.format("%-16s", "Account"));
    printWriter.write(String.format("%-19s", "Invoice"));
    printWriter.write(String.format("%-17s", "Invoice Date"));
    printWriter.write(String.format("%-13s", "Amount"));
    printWriter.write(String.format("%-16s", "Ship & Svc"));
    printWriter.write(String.format("%-12s" + "%n", "Total Amount"));
    printWriter.write("=========== ");                  // VendorId
    printWriter.write("============================= ");// Vendor Name
    printWriter.write("=========== ");                  // Voucher
    printWriter.write("=============== ");              // Account
    printWriter.write("================== ");           // Invoice
    printWriter.write("================ ");             // InvDate
    printWriter.write("============ ");                 // Amount
    printWriter.write("=============== ");              // Ship&Svc
    printWriter.write("============");                  // Total Amount
    printWriter.write("\n");
    printWriter.flush(); // stream out bytes

    // Write out report detail lines
    for (Vendor vendor : getVendors()) {
      if (vendor.getAccountNumber() != null) {
        // Write the vendor code
        printWriter.write(String.format("%-12s",
                vendor.getVendorCode()));

        // Check for a blank vendor name
        if (vendor.getVendorName().trim().length() > 0) {
          // Write the vendor name
          printWriter.write(String.format("%-30s",
                  vendor.getVendorName().substring(0, 28)));
        } else {
          // Write a no Vendor name message
          printWriter.write(String.format("%-30s", "[Vendor Name "
                  + "not provided]"));
        }

        // Write the voucher
        printWriter.write(String.format("%-12s",
                vendor.getVoucher().trim()));

        // Write the account number
        printWriter.write(String.format("%-16s",
                vendor.getAccountNumber().trim()));

        // Write the invoice number
        printWriter.write(String.format("%-19s",
                vendor.getInvoiceNumber().trim()));

        // Write the invoice date
        printWriter.write(String.format("%-16s",
                Util.reportDateFormater(
                        vendor.getInvoiceDate()).trim()));

        // Write the amount
        printWriter.write(String.format("%13s",
                currencyFormatter.format(vendor.getInvoiceAmount())));

        // Write the ship & svc
        printWriter.write(String.format("%16s",
                currencyFormatter.format(vendor.getShippingAmount()
                        + vendor.getDiscountAmount())));

        // Write the total amount
        printWriter.write(String.format("%13s" + "%n",
                currencyFormatter.format((vendor.calcTotalAmount()))));

        printWriter.flush(); // stream out bytes

        // Add the Vendor's total to the running total
        totalAmount += vendor.calcTotalAmount();
      }
    }

    // Write the footer
    printWriter.write(String.format(new String(new char[147])
            .replace('\0', '=')));

    // Write the total amount
    printWriter.write(String.format("\nTotal: %140s" + "%n",
            currencyFormatter.format(totalAmount)));

    printWriter.flush(); // Stream out bytes
  }

  /**
   * @return The report as a String
   * @throws ParseException
   * @throws IOException
   */
  public String printWorkDayReport() throws ParseException, IOException {
    Writer pw;
    ByteArrayOutputStream ops;

    ops = new ByteArrayOutputStream();
    pw = new PrintWriter(ops, true);
    printWorkDayReport(pw); // Overload

    return ops.toString();
  }

  /**
   * Write out the data report to file or console
   *
   * @param printWriter The Writer
   * @throws ParseException
   * @throws IOException
   */
  public void printLibraryReport(Writer printWriter)
          throws ParseException, IOException {
    double groupTotalAmount = 0.0;   // Running total
    double voucherTotalAmount;

    // If the report is writing to the console, add a divider to
    // distinguish the reports from each other
    if (getOutputDevice().equalsIgnoreCase("console")) {
      printWriter.write("\n"
              + new String(new char[147]).replace('\0', '=') + "\n\n");
    }

    // Format and print the header
    printWriter.write(formatOutputHeader());

    // Flush stream
    printWriter.flush();

    // Iterate through all the vendors with more than one invoice
    for (Vendor vendor : getVendors()) {
      if (vendor.hasInvoices()) {
        String invoiceNumber = vendor.getInvoiceNumber();
        String account = vendor.getAccountNumber();
        String paidDate = vendor.getInvoiceDate();

        final String DESCRIPTION = "LIBRARY VOUCHER ";

        // 140 = positive
        // 150 = negative
        int typeCode;

        // Increment group total
        voucherTotalAmount = vendor.calcTotalAmount();
        groupTotalAmount += voucherTotalAmount;

        // Set the type code based on the total amount
        if (voucherTotalAmount > 0) {
          typeCode = 140;
        } else {
          typeCode = 150;
          voucherTotalAmount *= -1;
        }

        // Write the current vendor to the report
        printWriter.write("\n" + typeCode);                             // Type Code
        printWriter.write(vendor.getVendorCode());                      // Vendor Code
        printWriter.write("0" + vendor.getVoucher());                   // Voucher (with a 0)
        printWriter.write(String.format("%-17s", account));             // Account Number
        printWriter.write(invoiceNumber);                               // Invoice Number
        printWriter.write(DESCRIPTION);                                 // Description
        printWriter.write(String.format("%-17s", vendor.getVoucher())); // Voucher
        printWriter.write(paidDate);                                    // Paid Date
        printWriter.write(Util.dateFormatter(new Date()));              // Today's Date
        printWriter.write(String.format("%011.0f", // Total Amount
                voucherTotalAmount * 100));

        printWriter.flush(); // Stream out bytes
      } else {
        // Ignore a Vendor that does not have invoices
        LOG.log(Level.WARNING, "Vendor: {0}: {1} has no invoices.",
                new Object[]{
                  vendor.getVendorCode(),
                  vendor.getVendorName().trim()
                });
      }
    }

    // Write a blank line at the bottom of the file
    printWriter.write("\n");

    // Print a success message with the total amount
    // and number of vendors
    System.out.println("\n\nExecution completed successfully with no errors.");
    System.out.println("================================================");
    System.out.println("Group Total Amount: "
            + currencyFormatter.format((groupTotalAmount)));

    System.out.println("Vendor Count: " + getVendorSize());
  }

  /**
   * @return The report as a String
   * @throws ParseException
   * @throws IOException
   */
  public String printLibraryReport() throws ParseException, IOException {
    Writer pw;
    ByteArrayOutputStream ops;

    ops = new ByteArrayOutputStream();
    pw = new PrintWriter(ops, true);
    printLibraryReport(pw); // Overload

    return ops.toString();
  }

  /**
   * @param properties The properties to set
   */
  private void setProperties(Properties properties) {
    this.properties = properties;
  }

  /**
   * @param currentVendor The currentVendor to set
   */
  private void setCurrentVendor(Vendor currentVendor) {
    this.currentVendor = currentVendor;
  }

  /**
   * @param inputFile The inputFile to set
   */
  private void setInputFile(File inputFile) {
    this.inputFile = inputFile;
  }

  /**
   * Set the log level from the properties file
   *
   * @param LOG The logger
   * @param logLevel The level the log should show
   * @throws NumberFormatException
   * @throws AssertionError
   * @throws SecurityException
   */
  private void setLogLevel(Logger LOG, int logLevel)
          throws NumberFormatException, SecurityException, AssertionError {
    try {
      switch (logLevel) {
        // OFF(3200), SEVERE(1000), WARNING(900), INFO(800),
        // CONFIG(700), FINE(500), FINER(400), FINEST(300), ALL(-3200)
        case -3200: // ALL
          LOG.setLevel(Level.ALL);
          break;
        case 300: // FINEST
          LOG.setLevel(Level.FINEST);
          break;
        case 400: // FINER
          LOG.setLevel(Level.FINER);
          break;
        case 500: // FINE
          LOG.setLevel(Level.FINE);
          break;
        case 700: // CONFIG
          LOG.setLevel(Level.CONFIG);
          break;
        case 800: // INFO
          LOG.setLevel(Level.INFO);
          break;
        case 900: // WARNING
          LOG.setLevel(Level.WARNING);
          break;
        case 1000: // SEVERE
          LOG.setLevel(Level.SEVERE);
          break;
        case 3200: // OFF
          LOG.setLevel(Level.OFF);
          break;
        default: // INFO
          LOG.setLevel(Level.INFO);
          throw new AssertionError();
      }
    } catch (NumberFormatException | SecurityException e) {
      LOG.throwing(this.getClass().getName(), "setLogLevel()", e);
      throw e;
    }
  }

  /**
   * @param outputDevice The output device
   */
  private void setOutputDevice(String outputDevice) {
    this.outputDevice = outputDevice;
  }

  /**
   * @return The properties
   */
  private Properties getProperties() {
    return properties;
  }

  /**
   * @return The currentVendor
   */
  private Vendor getCurrentVendor() {
    return currentVendor;
  }

  /**
   * @return The input file
   */
  private File getInputFile() {
    return inputFile;
  }

  /**
   * @return The list of vendors
   */
  private List<Vendor> getVendors() {
    return vendors;
  }

  /**
   * @return The list of records
   */
  private List<Record> getRecords() {
    return records;
  }

  /**
   * @return Amount of vendors with at least one invoice
   */
  private int getVendorSize() {
    return getVendors().size();
  }

  /**
   * @return The output device
   */
  private String getOutputDevice() {
    return outputDevice;
  }

  /**
   * @return The group total of all the invoices
   */
  private double calcGroupTotal() // WAS A LONG
  {
    double total = 0;

    // Iterate through Vendors
    for (Vendor vendor : getVendors()) {
      // Sum all its Invoices
      total += vendor.calcTotalAmount();
    }

    return total;
  }

  /**
   * Calculate how many errors there are in Vendor and Invoice classes
   *
   * @return The error count
   */
  public int getErrorCount() {
    int errorCount = 0;

    // Iterate through vendors
    for (Vendor vendor : getVendors()) {
      // Check for errors and increment the count
      if (vendor.hasErrors()) {
        errorCount += vendor.getErrorCount();
      }
    }

    return errorCount;
  }

  /**
   * @return True if there are errors
   */
  public boolean hasErrors() {
    return getErrorCount() > 0;
  }

  /**
   * Clean out the pcard and wire records from the Vendor list
   */
  private void cleanVendorList() {
    // Iterate though the Vendors and verify it has at least one Invoice
    for (Vendor vendor : getVendors()) {
      if (!vendor.hasInvoices()) {
        getVendors().remove(vendor);
      }
    }
  }

  /**
   * Create the header for the library report
   *
   * @return The LibraryReport header
   * @throws ParseException
   */
  private String formatOutputHeader() throws ParseException {
    // Constants
    final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
    final String DESCRIPTION = "ELECTRONIC LIBRARY VOUCHERS        NAP";
    final String END_SPACE = new String(new char[157]).replace('\0', ' ');

    // Difference in days between 5/30/2013 and the current date
    int differenceInDays;

    // The header to write
    String header;

    // Format the valid record size
    String recordCount = String.format("%05d", getBatchCount());

    // Dates
    Date startDate;
    Date currentDate = new Date();
    DateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss");

    // Create and format startDate
    try {
      startDate = formatter.parse("20130530 00:00:00");
    } catch (ParseException e) {
      LOG.throwing(this.getClass().getName(), "formatOutputHeader()", e);
      throw e;
    }

    // Calculate amount of days since startDate
    differenceInDays = (int) ((currentDate.getTime() - startDate.getTime()) / DAY_IN_MILLIS);

    // Construct header
    header = "$$$LNV"
            + differenceInDays
            + Util.dateFormatter(currentDate)
            + DESCRIPTION
            + recordCount
            + String.format("%011.0f", (calcGroupTotal() * 100))
            + new String(new char[12]).replace('\0', ' ')
            + "00030YBYY"
            + END_SPACE;

    return header;
  }

  /**
   * @return the batchCount
   */
  private int getBatchCount() {
    return batchCount;
  }

  /**
   * @param batchCount the batchCount to set
   */
  private void setBatchCount(int batchCount) {
    this.batchCount = batchCount;
  }

  /**
   * @return the batchAmount
   */
  private double getBatchAmount() {
    return batchAmount;
  }

  /**
   * @param batchAmount the batchAmount to set
   */
  private void setBatchAmount(double batchAmount) {
    this.batchAmount = batchAmount;
  }
}
