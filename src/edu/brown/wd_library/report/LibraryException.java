package edu.brown.wd_library.report;

import java.io.IOException;
import java.io.Writer;

/**
 * This class will create and store errors
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
final class LibraryException extends Exception
{
    private String message;
    private String record; // Reserved for a non instantiated Record

    private ErrorType errorType;
    private Vendor vendor;
    private Invoice invoice;

    private int lineNumber;

    public enum ErrorType
    {
        // Null Invoice
        INVALID_INVOICE,

        VOUCHERS_DO_NOT_MATCH,

        // Not numerics
        AMOUNT_NOT_NUMERIC,
        SHIPPING_NOT_NUMERIC,
        DISCOUNT_NOT_NUMERIC,
        VOUCHER_NOT_NUMERIC,
        ACCOUNT_NOT_NUMERIC,

        // Invalid lengths
        INVALID_RECORD,
        INVALID_VOUCHER,
        INVALID_ACCOUNT,
        INVALID_INVOICE_DATE,
        INVALID_INVOICE_NUMBER,
        INVALID_AMOUNT,
        INVALID_SHIPPING,
        INVALID_DISCOUNT,

        INVALID_RECORD_TYPE,
        INVALID_VENDOR_CODE,
        INVALID_VENDOR_NAME;
    }

    LibraryException()
    {
        super();
    }

    /**
     * @param message The message to set
     */
    LibraryException(String message)
    {
        super(message);
    }

    /**
     * This is used for invalid length errors and
     * not numeric errors
     *
     * @param message The error message to set
     * @param invoice The invoice to set
     * @param errorType The error type to set
     */
    LibraryException(String message, Invoice invoice, ErrorType errorType)
    {
        setMessage(message);
        setLineNumber(invoice.getLineNumber());
        setErrorType(errorType);
        setInvoice(invoice);
    }

    /**
     * This is used for invalid length errors and not numeric errors
     *
     * @param message The error message to set
     * @param vendor The vendor to set
     * @param errorType The error type to set
     */
    LibraryException(String message, Vendor vendor, ErrorType errorType)
    {
        setMessage(message);
        setLineNumber(vendor.getLineNumber());
        setErrorType(errorType);
        setVendor(vendor);
    }

    /**
     * This is used when a vendor's voucher does not match
     * the invoice's voucher
     *
     * @param message The error message to set
     * @param vendor The vendor to set
     * @param invoice The invoice to set
     * @param errorType The error type to set
     */
    LibraryException(String message, Vendor vendor, Invoice invoice,
            ErrorType errorType)
    {
        setVendor(vendor);
        setInvoice(invoice);
        setLineNumber(getInvoice().getLineNumber());
        setMessage(message);
        setErrorType(errorType);
    }

    // Mutator methods

    /**
     * @param message The error message to set
     */
    private void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * @param vendor The Vendor to set
     */
    private void setVendor(Vendor vendor)
    {
        this.vendor = vendor;
    }

    /**
     * @param invoice The Invoice to set
     */
    private void setInvoice(Invoice invoice)
    {
        this.invoice = invoice;
    }

    /**
     * @param errorType The error type to set
     */
    private void setErrorType(ErrorType errorType)
    {
        this.errorType = errorType;
    }

    /**
     * @param lineNumber The lineNumber to set
     */
    private void setLineNumber(int lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    // Accessor methods

    /**
     * @return The error message
     */
    @Override
    public String getMessage()
    {
        return message;
    }

    /**
     * @return The record
     */
    private String getRecord()
    {
        return record;
    }

    /**
     * @return The vendor
     */
    private Vendor getVendor()
    {
        return vendor;
    }

    /**
     * @return The invoice
     */
    private Invoice getInvoice()
    {
        return invoice;
    }

    /**
     * @return The error type
     */
    private ErrorType getErrorType()
    {
        return errorType;
    }

    /**
     * @return The line number
     */
    private int getLineNumber()
    {
        return lineNumber;
    }

    /**
     * Write this error to file or console
     *
     * @param printWriter The errors Writer
     * @param count The error count
     */
    void print(Writer printWriter, int[] count) throws IOException
    {
        // Print the error count and header
        printWriter.write("Error " + count[0] + ":\n");
        printWriter.write(new String(new char[300]).replace('\0', '='));
        printWriter.write("\n");

        // Write the line number if provided
        if (getLineNumber() > 0)
        {
            //StringFormatter formatter = new StringFormatter("");
            printWriter.write(String.format("%-16s%d\n", "Line Number:", getLineNumber()));
        }

        // Write the error type and message
        printWriter.write(String.format("%-16s%s\n", "Error Type:", getErrorType()));
        printWriter.write(String.format("%-15s %s\n", "Error Message:", getMessage()));

        // Write the Record if provided
        if (getRecord() != null)
        {
            printWriter.write(String.format("%-16s%s\n", "Record:", getRecord()));
        }

        // Write the Vendor if provided
        if (getVendor() != null)
        {
            printWriter.write(String.format("%-16s%s\n", "Vendor:",
                    getVendor().toString()));
        }

        // Write the Invoice if provided
        if (getInvoice() != null)
        {
            printWriter.write(String.format("%-16s%s\n", "Invoice:",
                    getInvoice().toString()));
        }

        // Write the footer
        printWriter.write(String.format(new String(new char[300]).replace('\0', '=')));
        printWriter.write("\n\n");

        // Increment the error count
        count[0]++;
    }

    /**
     * Convert the error into a String
     */
    @Override
    public String toString()
    {
        String message = "\n";

        // Add the line number if provided
        if (getLineNumber() > 0)
        {
            message += String.format("%-16s%d\n", "Line Number:", getLineNumber());
        }

        // Add the error type and message
        message += String.format("%-16s%s\n", "Error Type:", getErrorType());
        message += String.format("%-15s %s\n", "Error Message:", getMessage());

        // Add the Record if provided
        if (getRecord() != null)
        {
            message += String.format("%-16s%s\n", "Record:", getRecord());
        }

        // Add the Vendor if provided
        if (getVendor() != null)
        {
            message += String.format("%-16s%s\n", "Vendor:", getVendor().toString());
        }

        // Add the Invoice if provided
        if (getInvoice() != null)
        {
            message += String.format("%-16s%s\n", "Invoice:", getInvoice().toString());
        }

        return message;
    }
}
