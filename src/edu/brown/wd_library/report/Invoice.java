package edu.brown.wd_library.report;

import edu.brown.wd_library.report.LibraryException.ErrorType;
import edu.brown.wd_library.report.Util.ConvertType;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will take in a record/invoice and store its data
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
class Invoice
{
    private static final Logger LOG = Logger
            .getLogger(WDLibraryProcessReport.class.getName());

    private final Record record;    // Current record

    private String voucher;         // Voucher (Only used to match with vendor's voucher)
    private String account;         // Account number
    private String invoiceNumber;   // Invoice number
    private String invoiceDate;     // Invoice date

    private double amount;          // Amount of purchase
    private double shipping;        // Cost of shipping
    private double discount;        // Discount amount if applicable

    // List of Invoice errors
    private final List<LibraryException> errors;

    Invoice(Record record) throws ParseException
    {
        this.record = record;
        errors = new CopyOnWriteArrayList<>();

        // Check that the record length is valid
        checkRecordLength(record);

        // Set the Invoice fields from the record
        setVoucher(record.toString().substring(0, 7));
        setAccount(record.toString().substring(20, 40));
        setInvoiceNumber(record.toString().substring(56, 72));
        setInvoiceDate(record.toString().substring(48, 56));
        setAmount(record.toString().substring(72, 86));
        setShipping(record.toString().substring(114, 128));
        setDiscount(record.toString().substring(129, 142));
    }

    // Mutator methods

    /**
     * Validate and set the voucher
     *
     * @param voucher The voucher to set
     * @throws LibraryException
     */
    private void setVoucher(String voucher)
    {
        String errorMessage =
                "Invalid Voucher Length: <" + voucher + ">";

        String notNumericErrorMessage =
                "Expected numeric Voucher value, but saw <"
                    + voucher.trim() + ">";

        int length = voucher.length();

        // Check for a valid length
        if (length <= 7 && length > 0)
        {
            // Check for numeric
            if (Util.isNumeric(voucher.trim()))
            {
                // Set the voucher
                this.voucher = voucher.trim();
            } else
            {
                // Add the numeric error to the Invoice's errors list
                LibraryException e =
                        new LibraryException(notNumericErrorMessage,
                                this, ErrorType.VOUCHER_NOT_NUMERIC);

                LOG.log(Level.SEVERE,
                        "**** Voucher value is not numeric: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } else
        {
            // Add the voucher length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                         this, ErrorType.INVALID_VOUCHER);

            LOG.log(Level.SEVERE, "**** Invalid Voucher Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the account number
     *
     * @param account New account number
     */
    private void setAccount(String account)
    {
        String errorMessage =
                "Invalid Account Length: <" + account + ">";

        String notNumericErrorMessage =
                "Account Number not numeric: <" + account + ">";

        int length = account.length();

        // Check for a valid length
        if (length <= 20 && length > 0)
        {
            // Set the account number
            if (Util.isNumeric(account.trim()))
            {
                this.account = account.trim();
            } else
            {
                // Add the numeric error to the Invoice's errors list
                LibraryException e =
                        new LibraryException(notNumericErrorMessage,
                                this, ErrorType.ACCOUNT_NOT_NUMERIC);

                LOG.log(Level.SEVERE, "**** Account Number not numeric: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } else
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                            this, ErrorType.INVALID_ACCOUNT);

            LOG.log(Level.SEVERE, "**** Invalid Account Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the invoice date
     *
     * @param invoiceDate The invoice date to set
     * @throws LibraryException
     */
    private void setInvoiceDate(String invoiceDate) throws ParseException
    {
        String errorMessage =
                "Invalid Invoice Date Length: <" + invoiceDate + ">";

        String futureDateErrorMessage =
                "Future Dates are not valid <" + invoiceDate + ">";

        String unparsableDateErrorMessage =
                "Unparsable Invoice Date: <" + invoiceDate + ">";

        int length = invoiceDate.length();

        // Check for a valid length
        if (length <= 8 && length > 0)
        {
            try
            {
                // Set the invoice date
                this.invoiceDate = Util.dateFormatter(invoiceDate.trim());

                // Check for future date
                if (!isValidDate(getInvoiceDate()))
                {
                    // Add the invalid date error to the Invoice's errors list
                    LibraryException e =
                            new LibraryException(futureDateErrorMessage,
                                    this, ErrorType.INVALID_INVOICE_DATE);

                    LOG.log(Level.SEVERE,
                            "**** Unparsable Invoice Date: {0}****\n",
                            e.toString());

                    getErrors().add(e);
                }
            } catch (ParseException e)
            {
                // Add the unparsable date error to the Invoice's errors list
                LibraryException ex =
                        new LibraryException(unparsableDateErrorMessage,
                                this, ErrorType.INVALID_INVOICE_DATE);

                LOG.log(Level.SEVERE,
                        "**** Unparsable Invoice Date: {0}****\n",
                        ex.toString());

                getErrors().add(ex);
            }
        } else
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                            this, ErrorType.INVALID_INVOICE_DATE);

            LOG.log(Level.SEVERE,
                    "**** Invalid Invoice Date Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the invoice number
     *
     * @param invoiceNumber The invoice number to set
     * @throws LibraryException
     */
    private void setInvoiceNumber(String invoiceNumber)
    {
        String errorMessage =
                "Invalid Invoice Number Length: <" + invoiceNumber + ">";

        int length = invoiceNumber.length();

        // Check for a valid length
        if (length <= 16 && length > 0)
        {
            this.invoiceNumber = invoiceNumber;
        } else
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                            this, ErrorType.INVALID_INVOICE_NUMBER);

            LOG.log(Level.SEVERE,
                    "**** Invalid Invoice Number Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the amount
     *
     * @param amount The amount to set
     * @throws LibraryException
     */
    private void setAmount(String amount)
    {
        String errorMessage =
                "Invalid Amount Length: <" + amount + ">";

        int length = amount.length();

        // Check for a valid length
        if (length <= 14 && length > 0)
        {
            try
            {
                // Set the amount
                this.amount = Util.convertToDouble(amount.trim(),
                        this, ConvertType.AMOUNT) / 100;
            } catch (LibraryException e)
            {
                // Add the numeric error to the Invoice's errors list
                LOG.log(Level.SEVERE, "**** Amount Not Numeric: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } else
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                            this, ErrorType.INVALID_AMOUNT);

            LOG.log(Level.SEVERE, "**** Invalid Amount Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the shipping amount
     *
     * @param shipping The shipping amount to set
     * @throws LibraryException
     */
    private void setShipping(String shipping)
    {
        String errorMessage =
                "Invalid Shipping Length: <" + shipping + ">";

        int length = shipping.length();

        // Check for a valid length
        if (length <= 14 && length > 0)
        {
            try
            {
                // Set the shipping
                this.shipping = Util.convertToDouble(shipping.trim(),
                        this, ConvertType.SHIPPING) / 100;
            } catch (LibraryException e)
            {
                // Add the numeric error to the Invoice's errors list
                LOG.log(Level.SEVERE, "**** Shipping Not Numeric: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } else
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                            this, ErrorType.INVALID_SHIPPING);

            LOG.log(Level.SEVERE, "**** Invalid Shipping Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Validate and set the discount amount
     *
     * @param discount The discount amount to set
     * @throws LibraryException
     */
    private void setDiscount(String discount)
    {
        String errorMessage =
                "Invalid Discount Length: <" + discount + ">";

        int length =  discount.length();

        // Check for a valid length
        if (length <= 14 && length > 0)
        {
            try
            {
                // Set the discount
                this.discount = Util.convertToDouble(discount.trim(),
                        this, ConvertType.DISCOUNT) / 100;
            } catch (LibraryException e)
            {
                // Add the numeric error to the Invoice's errors list
                LOG.log(Level.SEVERE, "**** Discount Not Numeric: {0}****\n",
                        e.toString());

                getErrors().add(e);
            }
        } else
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException(errorMessage,
                            this, ErrorType.INVALID_DISCOUNT);

            LOG.log(Level.SEVERE, "**** Invalid Discount Length: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    // Accessor Methods

    /**
     * @return The voucher
     */
    String getVoucher()
    {
        return voucher;
    }

    /**
     * @return The account number
     */
    String getAccount()
    {
        return account;
    }

    /**
     * @return The invoice date
     */
    String getInvoiceDate()
    {
        return invoiceDate;
    }

    /**
     * @return The invoice number
     */
    String getInvoiceNumber()
    {
        return invoiceNumber;
    }

    /**
     * @return The amount
     */
    double getAmount()
    {
        return amount;
    }

    /**
     * @return The shipping cost
     */
    double getShipping()
    {
        return shipping;
    }

    /**
     * @return The discount
     */
    double getDiscount()
    {
        return discount;
    }

    /**
     * @return The record
     */
    private Record getRecord()
    {
        return record;
    }

    /**
     * @return The line number
     */
    int getLineNumber()
    {
        return getRecord().getLineNumber();
    }

    /**
     * @return The errors
     */
    private List<LibraryException> getErrors()
    {
        return errors;
    }

    /**
     * @return The amount of errors
     */
    int getErrorCount()
    {
        return getErrors().size();
    }

    /**
     * @return The total amount
     */
    double calcTotal()
    {
        return getAmount() + getShipping() + getDiscount();
    }

    /**
     * Print the errors to the report
     *
     * @param printWriter The errors Writer
     * @param count The error count
     */
    void printErrors(Writer printWriter, int[] count) throws IOException
    {
        for (LibraryException error : getErrors())
        {
            error.print(printWriter, count);
        }
    }

    /**
     * @return If there are errors
     */
    boolean hasErrors()
    {
        return getErrors().size() > 0;
    }

    /**
     * Check the record for a valid length
     *
     * @param record The Record
     */
    private void checkRecordLength(Record record)
    {
        if (!record.isValid())
        {
            // Add the length error to the Invoice's errors list
            LibraryException e =
                    new LibraryException("Record length larger than 250",
                        this, ErrorType.INVALID_RECORD);

            LOG.log(Level.SEVERE,
                    "**** Record length larger than 250: {0}****\n",
                    e.toString());

            getErrors().add(e);
        }
    }

    /**
     * Test for a future date
     *
     * @param invoiceDate The current date
     * @return True if the date is valid
     */
    private boolean isValidDate(String invoiceDate)
    {
        String currentDate = Util.dateFormatter(new Date());

        return invoiceDate.compareTo(currentDate) <= 0;
    }

    @Override
    /**
     * @return The record
     */
    public String toString()
    {
        return getRecord().toString();
    }
}
