package edu.brown.wd_library.report;

import edu.brown.wd_library.report.LibraryException.ErrorType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class holds static utility methods.
 *
 * @author Scott C. Tavares<scott_tavares@brown.edu;stavares@thecityside.com>
 * @version 1.0.2
 */
class Util
{
    private static final Logger LOG = Logger
            .getLogger(WDLibraryProcessReport.class.getName());

    public enum ConvertType
    {
        AMOUNT,
        SHIPPING,
        DISCOUNT
    }

    /**
     * Check if the string value is numeric
     *
     * @param value The value to check
     * @return If the value is numeric(true/false)
     */
    static boolean isNumeric(String value)
    {
        return value.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Convert the stringValue to an integer
     *
     * @param stringValue The String to convert
     * @param invoice The Invoice record
     * @param convertType The type of convert
     * @return The converted integer
     * @throws LibraryException
     */
    static int convertToInt(String stringValue, Invoice invoice,
            ConvertType convertType) throws LibraryException
    {
        int intValue;

        // Create the error type and message
        String errorMessage = getErrorMessage(convertType, stringValue);
        ErrorType errorType = getErrorType(convertType);

        // Check for numeric
        if (isNumeric(stringValue))
        {
            // Set the integer value
            intValue = Integer.parseInt(stringValue);
        } else
        {
            // Throw a LibraryException that will be caught and added to an
            // errors list
            LOG.log(Level.SEVERE, "stringValue: {0} is not Numeric\n",
                    stringValue);

            throw new LibraryException(errorMessage, invoice, errorType);
        }

        return intValue;
    }

    /**
     * Convert the stringValue to an integer
     *
     * @param stringValue The String to convert
     * @param vendor The Vendor record
     * @param convertType The type of convert
     * @return The converted integer
     * @throws LibraryException
     */
    static int convertToInt(String stringValue, Vendor vendor,
            ConvertType convertType) throws LibraryException
    {
        int intValue;

        // Create the error type and message
        String errorMessage = getErrorMessage(convertType, stringValue);
        ErrorType errorType = getErrorType(convertType);

        if (isNumeric(stringValue))
        {
            intValue = Integer.parseInt(stringValue);
        } else
        {
            // Throw a LibraryException that will be caught and added to an
            // errors list
            LOG.log(Level.SEVERE, "stringValue: {0} is not Numeric\n",
                    stringValue);

            throw new LibraryException(errorMessage, vendor, errorType);
        }

        return intValue;
    }

    /**
     * Convert the stringValue to a double
     *
     * @param stringValue The String to convert
     * @param invoice The Invoice record
     * @param convertType The type of convert
     * @return The converted double
     * @throws LibraryException
     */
    static double convertToDouble(String stringValue,
            Invoice invoice, ConvertType convertType) throws LibraryException
    {
        double doubleValue;

        // Create the error type and message
        String errorMessage = getErrorMessage(convertType, stringValue);
        ErrorType errorType = getErrorType(convertType);

        // Check for numeric
        if (isNumeric(stringValue))
        {
            // Set the double value
            doubleValue = Double.parseDouble(stringValue);
        } else
        {
            // Throw a LibraryException that will be caught and added to an
            // errors list
            LOG.log(Level.SEVERE, "stringValue: {0} is not Numeric\n",
                    stringValue);

            throw new LibraryException(errorMessage, invoice, errorType);
        }

        return doubleValue;
    }

    /**
     * Convert the stringValue to a double
     *
     * @param stringValue The String to convert
     * @param vendor The Vendor record
     * @param convertType The type of convert
     * @return The converted double
     * @throws LibraryException
     */
    static double convertToDouble(String stringValue,
            Vendor vendor, ConvertType convertType) throws LibraryException
    {
        double doubleValue;

        // Create the error type and message
        String errorMessage = getErrorMessage(convertType, stringValue);
        ErrorType errorType = getErrorType(convertType);

        // Check for numeric
        if (isNumeric(stringValue))
        {
            doubleValue = Double.parseDouble(stringValue);
        } else
        {
            // Throw a LibraryException that will be caught and added to an
            // errors list
            LOG.log(Level.SEVERE, "stringValue: {0} is not Numeric\n",
                    stringValue);

            throw new LibraryException(errorMessage, vendor, errorType);
        }

        return doubleValue;
    }

    /**
     * This will take in a String date and convert it to a yyyyMMdd
     * format.
     *
     * @param date Unformatted String date
     * @return Formatted String date
     * @throws ParseException
     */
    static String dateFormatter(String date) throws ParseException
    {
        // Set up the formatters
        DateFormat preFormat = new SimpleDateFormat("MM-dd-yy");
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        try
        {
            // Set and format the date
            date = formatter.format(preFormat.parse(date));
        } catch (ParseException e)
        {
            // Throw a ParseException that will be caught and added to an
            // errors list
            LOG.log(Level.SEVERE, "Unparseable date: {0}\n", date);
            throw e;
        }

        return date;
    }

    /**
     * This will take in a Date Object and convert it to a yyyyMMdd
     * format.
     *
     * @param date Unformatted date
     * @return Formatted String date
     */
    static String dateFormatter(Date date)
    {
        // Set up the formatter
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        // Format the date
        return formatter.format(date);
    }

    /**
     * This will take a String date and convert it to a MM/dd/yyyy format for
     * the WorkDay report
     *
     * @param date Unformatted date
     * @return Formatted date
     * @throws ParseException
     */
	static String reportDateFormater(String date) throws ParseException
    {
        // Set up the formatters
        DateFormat preFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

        try
        {
            // Set and format the date
            date = formatter.format(preFormat.parse(date));
        } catch (ParseException e)
        {
            // Throw a ParseException that will be caught and added to an
            // errors list
            LOG.log(Level.SEVERE, "Unparseable date: {0}\n", date);
            throw e;
        }

        return date;
    }

	    /**
     * Get the error type from the convertType
     *
     * @param convertType The convert type
     * @return The error type based on the convert type
     */
	private static ErrorType getErrorType(ConvertType convertType)
	{
	    ErrorType type;

        // Get the error type
	    switch (convertType)
	    {
	        case AMOUNT:
	            type = ErrorType.AMOUNT_NOT_NUMERIC;
	            break;
	        case SHIPPING:
	            type = ErrorType.SHIPPING_NOT_NUMERIC;
	            break;
	        default:   // DISCOUNT
	            type = ErrorType.DISCOUNT_NOT_NUMERIC;
	    }

        return type;
	}

    /**
     * Create the error message for the convertTo methods
     *
     * @param convertType The convert type
     * @param stringValue The string value
     * @return The error message
     */
	private static String getErrorMessage(ConvertType convertType, String stringValue)
	{
	    return "Expected numeric " + (convertType == ConvertType.AMOUNT ? "Amount" :
               convertType == ConvertType.SHIPPING ? "Shipping" : "Discount")
               + " value, but saw <"
               + stringValue + ">";
	}
}
